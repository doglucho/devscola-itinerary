import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EjerciciosBasicosComponent } from './ejercicios-basicos.component';

describe('EjerciciosBasicosComponent', () => {
  let component: EjerciciosBasicosComponent;
  let fixture: ComponentFixture<EjerciciosBasicosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EjerciciosBasicosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EjerciciosBasicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
