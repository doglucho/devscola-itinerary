import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-command-line',
  templateUrl: './command-line.component.html',
  styleUrls: ['./command-line.component.scss']
})
export class CommandLineComponent {
  content: string = 'Command Line'
  selectedValue: any = [];
  
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
  }
}
