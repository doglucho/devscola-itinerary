import { AfterViewInit, Component, Renderer2 } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent {
  sidebarVisible?: boolean = false;
  items: MenuItem[] = [];
  selectedItem?: any = null;

  constructor() {}

  ngOnInit() {
    this.items = [
      {
        label: 'Command Line',
        icon: 'pi pi-bolt',
        routerLink: 'command-line',
        command: () => {
          this.onItemSelect({});
        },
      },
      {
        label: 'Git',
        icon: 'pi pi-bolt',
        routerLink: 'git',
        command: () => {
          this.onItemSelect({});
        }
      },
      {
        label: 'JavaScript',
        icon: 'pi pi-bolt',
        routerLink: 'javascript',
        command: () => {
          this.onItemSelect({});
        }
      },
      {
        label: 'Python',
        icon: 'pi pi-bolt',
        routerLink: 'python',
        command: () => {
          this.onItemSelect({});
        }
      },
      {
        label: 'Ruby',
        icon: 'pi pi-bolt',
        routerLink: 'ruby',
        command: () => {
          this.onItemSelect({});
        }
      },
      {
        label: 'Ejercicios Básicos de Programación',
        icon: 'pi pi-bolt',
        routerLink: 'ejercicios',
        command: () => {
          this.onItemSelect({});
        }
      },
      {
        label: 'Katas',
        icon: 'pi pi-bolt',
        routerLink: 'katas',
        command: () => {
          this.onItemSelect({});
        }
      },
      {
        label: 'HTML y CSS Introductorios',
        icon: 'pi pi-bolt',
        routerLink: 'html-css',
        command: () => {
          this.onItemSelect({});
        }
      }
    ];
  }

  openSidebar(): void {
    this.sidebarVisible = true;
  }

  onItemSelect(item: any): void {
    this.sidebarVisible = false;
    this.selectedItem = item;
  }
}
