import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KatasComponent } from './katas.component';

describe('KatasComponent', () => {
  let component: KatasComponent;
  let fixture: ComponentFixture<KatasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KatasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KatasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
