import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommandLineComponent } from './components/command-line/command-line.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { GitComponent } from './components/git/git.component';
import { JavascriptComponent } from './components/javascript/javascript.component';
import { PythonComponent } from './components/python/python.component';
import { RubyComponent } from './components/ruby/ruby.component';
import { EjerciciosBasicosComponent } from './components/ejercicios-basicos/ejercicios-basicos.component';
import { KatasComponent } from './components/katas/katas.component';
import { HtmlCssComponent } from './components/html-css/html-css.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard/command-line', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent, children: [
    { path: '', redirectTo: 'command-line', pathMatch: 'full' },
    { path: 'command-line', component: CommandLineComponent },
    { path: 'git', component: GitComponent },
    { path: 'javascript', component: JavascriptComponent },
    { path: 'python', component: PythonComponent },
    { path: 'ruby', component: RubyComponent },
    { path: 'ejercicios', component: EjerciciosBasicosComponent },
    { path: 'katas', component: KatasComponent },
    { path: 'html-css', component: HtmlCssComponent }
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
})

export class AppRoutingModule { }
