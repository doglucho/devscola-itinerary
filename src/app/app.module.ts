import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { SidebarModule } from 'primeng/sidebar';
import { ButtonModule } from 'primeng/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuModule } from 'primeng/menu';
import { CheckboxModule } from 'primeng/checkbox';

import { AppComponent } from './app.component';
import { CommandLineComponent } from './components/command-line/command-line.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { GitComponent } from './components/git/git.component';
import { JavascriptComponent } from './components/javascript/javascript.component';
import { PythonComponent } from './components/python/python.component';
import { RubyComponent } from './components/ruby/ruby.component';
import { EjerciciosBasicosComponent } from './components/ejercicios-basicos/ejercicios-basicos.component';
import { KatasComponent } from './components/katas/katas.component';
import { HtmlCssComponent } from './components/html-css/html-css.component';

@NgModule({
  declarations: [
    AppComponent,
    CommandLineComponent,
    DashboardComponent,
    GitComponent,
    JavascriptComponent,
    PythonComponent,
    RubyComponent,
    EjerciciosBasicosComponent,
    KatasComponent,
    HtmlCssComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SidebarModule,
    ButtonModule,
    BrowserAnimationsModule,
    MenuModule,
    CheckboxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
